package com.java1234.dao;

import com.java1234.model.User;

public interface UserDao {

	public User login(User user) throws Exception;
}
