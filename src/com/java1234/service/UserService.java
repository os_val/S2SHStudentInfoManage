package com.java1234.service;

import com.java1234.model.User;

public interface UserService {

	public User login(User user) throws Exception;
}
