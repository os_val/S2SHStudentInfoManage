/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.1.49-community : Database - db_studentinfo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_studentinfo` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_studentinfo`;

/*Table structure for table `t_grade` */

DROP TABLE IF EXISTS `t_grade`;

CREATE TABLE `t_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gradeName` varchar(20) DEFAULT NULL,
  `gradeDesc` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `t_grade` */

insert  into `t_grade`(`id`,`gradeName`,`gradeDesc`) values (1,'08计本','08计算机本科'),(2,'09计本','09计算机本科'),(29,'我们的唉','啊');

/*Table structure for table `t_student` */

DROP TABLE IF EXISTS `t_student`;

CREATE TABLE `t_student` (
  `stuId` int(11) NOT NULL AUTO_INCREMENT,
  `stuNo` varchar(20) DEFAULT NULL,
  `stuName` varchar(10) DEFAULT NULL,
  `sex` varchar(5) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gradeId` int(11) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `stuDesc` varchar(1000) DEFAULT NULL,
  `gradeName` varchar(255) DEFAULT NULL,
  `brithday` datetime DEFAULT NULL,
  PRIMARY KEY (`stuId`),
  KEY `FK_t_student` (`gradeId`),
  CONSTRAINT `FK_t_student` FOREIGN KEY (`gradeId`) REFERENCES `t_grade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/*Data for the table `t_student` */

insert  into `t_student`(`stuId`,`stuNo`,`stuName`,`sex`,`birthday`,`gradeId`,`email`,`stuDesc`,`gradeName`,`brithday`) values (2,'080606110','张三','男','1989-11-03',1,'31321@qq.com','Good',NULL,NULL),(3,'080606110','张三','男','1989-11-03',1,'31321@qq.com','Good',NULL,NULL),(4,'080606110','张三','男','1989-11-03',1,'31321@qq.com','Good',NULL,NULL),(5,'0806061102','张三2','男','1989-11-02',29,'313222@qq.com','Good222',NULL,NULL),(9,'080606110','张三','男','1989-11-03',1,'31321@qq.com','Good',NULL,NULL),(13,'080606110','张三','男','1988-11-03',1,'31321@qq.com','Good',NULL,NULL),(14,'080606112','张三2','女','1989-11-02',2,'3132122@qq.com','Good2',NULL,NULL),(15,'08060611011','张三2','女','1989-11-02',2,'31321222@qq.com','Good22',NULL,NULL),(16,'080606110','张三','男','1989-11-03',1,'31321@qq.com','Good',NULL,NULL),(17,'080606110','张三','男','1989-11-03',1,'31321@qq.com','Good',NULL,NULL),(18,'090606119','王小美','女','1990-11-03',1,'3112121@qq.com','Good Girls11',NULL,NULL),(19,'090606119','王小美','女','1990-11-03',2,'3112121@qq.com','Good Girls',NULL,NULL),(20,'090606119','王小美','女','1990-11-03',1,'3112121@qq.com','Good Girls',NULL,NULL),(21,'090606119','王小美','女','1990-11-03',2,'3112121@qq.com','Good Girls23',NULL,NULL),(38,'11','AAA','女','2013-05-09',1,'12@qq.com','12',NULL,NULL),(39,'3388','死三八','男','2013-05-08',29,'12@qq.com','是',NULL,NULL),(43,'135222','王二小22','男','2013-05-23',2,'322221@qq.com','我么22',NULL,NULL),(44,'12345677','你好77','女','2013-05-02',2,'321777@qq.com','哈哈是77',NULL,NULL),(45,'221221333','七里香33','男','2013-06-04',2,'32331@qq.com','都是33',NULL,NULL);

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`userName`,`password`) values (1,'java1234','123456');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
